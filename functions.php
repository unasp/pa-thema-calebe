<?php
/** Start the engine */
require_once( get_template_directory() . '/lib/init.php' );

/** Create additional color style options */
//add_theme_support( 'genesis-style-selector', array( 'associate-gray' => 'Gray', 'associate-green' => 'Green', 'associate-red' => 'Red') );

/** Child theme (do not remove) */
define( 'CHILD_THEME_NAME', 'Associate Theme' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/themes/associate' );

$content_width = apply_filters( 'content_width', 580, 0, 910 );

/** Unregister 3-column site layouts */
genesis_unregister_layout( 'content-sidebar-sidebar' );
genesis_unregister_layout( 'sidebar-sidebar-content' );
genesis_unregister_layout( 'sidebar-content-sidebar' );

/** Add new featured image sizes */
add_image_size( 'home-bottom', 150, 130, TRUE );
add_image_size( 'home-middle', 287, 120, TRUE );
add_image_size( 'thumb_home', 95, 65, TRUE );


add_filter( 'body_class', 'add_body_class' );
function add_body_class( $classes ) {
	
	$category = get_the_category(); 
	//$category[0]->cat_name;
    $classes[] = $category[0]->category_nicename ;
    return $classes;
}

remove_action( 'genesis_before_post_content', 'genesis_post_info' );


/** Customize search form input button text */
add_filter( 'genesis_search_button_text', 'custom_search_button_text' );
function custom_search_button_text($text) {
    return esc_attr('');
}

/** Customize search form input box text */
add_filter( 'genesis_search_text', 'custom_search_text' );
function custom_search_text($text) {
    return esc_attr('');
}


/** Add suport for custom background */
add_custom_background();

/** Add support for custom header */
//add_theme_support( 'genesis-custom-header', array( 'width' => 960, 'height' => 120, 'textcolor' => 'ffffff', 'admin_header_callback' => 'associate_admin_style' ) );

/** Add support for structural wraps */
add_theme_support( 'genesis-structural-wraps', array( 'header', 'nav', 'subnav', 'inner', 'footer-widgets', 'footer' ) );

/** Add support for 3-column footer widgets */
add_theme_support( 'genesis-footer-widgets', 3 );

/** Register widget areas */
genesis_register_sidebar( array(
	'id'			=> 'featured',
	'name'			=> __( 'Featured', 'associate' ),
	'description'	=> __( 'This is the featured section.', 'associate' ),
) );
genesis_register_sidebar( array(
	'id'			=> 'home-middle-1',
	'name'			=> __( 'Home Middle #1', 'associate' ),
	'description'	=> __( 'This is the first column of the home middle section.', 'associate' ),
) );
genesis_register_sidebar( array(
	'id'			=> 'home-middle-2',
	'name'			=> __( 'Home Middle #2', 'associate' ),
	'description'	=> __( 'This is the second column of the home middle section.', 'associate' ),
) );
genesis_register_sidebar( array(
	'id'			=> 'home-middle-3',
	'name'			=> __( 'Home Middle #3', 'associate' ),
	'description'	=> __( 'This is the third column of the home middle section.', 'associate' ),
) );
genesis_register_sidebar( array(
	'id'			=> 'home-middle-4',
	'name'			=> __( 'Home Middle #4', 'associate' ),
	'description'	=> __( 'This is the third column of the home middle section.', 'associate' ),
) );
genesis_register_sidebar( array(
	'id'			=> 'home-bottom-1',
	'name'			=> __( 'Home Bottom #1', 'associate' ),
	'description'	=> __( 'This is the first column of the home bottom section.', 'associate' ),
) );
genesis_register_sidebar( array(
	'id'			=> 'home-bottom-2',
	'name'			=> __( 'Home Bottom #2', 'associate' ),
	'description'	=> __( 'This is the second column of the home bottom section.', 'associate' ),
) );
genesis_register_sidebar( array(
	'id'			=> 'home-bottom-3',
	'name'			=> __( 'Home Bottom #3', 'associate' ),
	'description'	=> __( 'This is the second column of the home bottom section.', 'associate' ),
) );